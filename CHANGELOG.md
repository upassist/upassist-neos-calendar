# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [8.0.0](https://bitbucket.org/upassist/upassist-neos-calendar/compare/7.3.1...8.0.0) (2022-10-17)

### [7.3.1](https://bitbucket.org/upassist/upassist-neos-calendar/compare/1.0.2...7.3.1) (2022-10-10)

### [1.0.2](https://bitbucket.org/upassist/upassist-neos-calendar/compare/1.0.1...1.0.2) (2022-10-10)

### [1.0.1](https://bitbucket.org/upassist/upassist-neos-calendar/compare/7.3.0...1.0.1) (2022-10-10)
